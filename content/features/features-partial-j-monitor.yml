---
  monitor:
    content: Monitor
    copy_media:
      header: Monitor
      aos_animation: fade-up
      aos_duration: 500
      icon:
        name: monitor-alt
        alt: Monitor Icon
        variant: marketing
      subtitle: Help reduce the severity and frequency of incidents.
      text: Get feedback and the tools to help you reduce the severity and frequency of incidents so that you can release software frequently with confidence.
      categories:
        - Incident Management
        - On-call Schedule Management
        - Error Tracking
  feature_content:
    products_category:
      products:
        # Incident Management
        - title: IT Incidents
          categories:
            - Incident Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Triage and respond to IT incidents in GitLab. Open incidents manually or automatically for triggered alerts. Customize using issue templates.
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/incidents.html
            text: Documentation
            data_ga_name: IT Incidents
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Embedded metrics in incidents
          categories:
            - Incident Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Embed Prometheus and Grafana metrics charts in incidents
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/incidents.html#embed-metrics-in-incidents
            text: Documentation
            data_ga_name: Embedded metrics in incidents
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Generic HTTP Endpoint
          categories:
            - Incident Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Send alerts from any monitoring tool to a HTTP endpoint with the option to automatically create incidents.
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/alert_integrations.html#generic-http-endpoint
            text: Documentation
            data_ga_name: Generic HTTP Endpoint
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Status Page
          categories:
            - Incident Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Deploy a static web page to communicate with stakeholders during an incident. Push updates to the Status Page directly from the incident.
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/status_page.html
            text: Documentation
            data_ga_name: Status Page
            data_ga_location: body
          saas:
            - ultimate
          self_managed:
            - ultimate
        - title: Alerts
          categories:
            - Incident Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Aggregate and triage IT alerts from external services with the option to promote to incidents.
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/alerts.html
            text: Documentation
            data_ga_name: Alerts
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Alert Notifications
          categories:
            - Incident Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Get notified via email or Slack for triggered alerts
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/paging.html
            text: Documentation
            data_ga_name: Alert Notifications
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate
        - title: Service Level Agreement countdown timer
          categories:
            - Incident Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Track time for active incidents to ensure you meet your customer SLAs.
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/incidents.html#service-level-agreement-countdown-timer
            text: Documentation
            data_ga_name: Service Level Agreement countdown timer
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: On-call Schedule Management
          categories:
            - Incident Management
            - On-call Schedule Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Create schedules for responders to rotate on-call responsibilities.
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html
            text: Documentation
            data_ga_name: On-call Schedule Management
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        - title: Escalation Policies
          categories:
            - Incident Management
            - On-call Schedule Management
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: Automatically escalate and page on-call responders to safeguard against missed alerts.
          link:
            href: https://docs.gitlab.com/ee/operations/incident_management/escalation_policies.html
            text: Documentation
            data_ga_name: Escalation Policies
            data_ga_location: body
          saas:
            - premium
            - ultimate
          self_managed:
            - premium
            - ultimate
        # Error Tracking
        - title: Error tracking
          categories:
            - Error Tracking
          aos_animation: fade-up
          aos_duration: 500
          shadow_hover: true
          text: View Sentry errors directly within GitLab.
          link:
            href: https://docs.gitlab.com/ee/operations/error_tracking.html
            text: Documentation
            data_ga_name: Error tracking
            data_ga_location: body
          saas:
            - free
            - premium
            - ultimate
          self_managed:
            - free
            - premium
            - ultimate

