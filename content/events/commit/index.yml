---
  title: "GitLab Commit: Take Your DevOps Results to the Next Level"
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  image_title: /nuxt-images/open-graph/commit2022_opengraph.png
  image_alt: GitLab Commit 2022 - Level Up Your DevOps Results
  twitter_image: /nuxt-images/open-graph/commit2022_opengraph.png

  navigation:
    links:
    - text: Locations
      href: "#locations"
    - text: Schedules
      href: "#schedules"
    - text: Speakers
      href: "#speakers"
    - text: FAQ
      href: "#faq"
    - text: Sponsors
      href: "#sponsors"

  hero:
    subtitle: Take your DevOps results to the next level
    text: |
      Now’s the time to come together for fresh ideas to fuel software innovation, embrace collaboration, and strengthen security at your organization. Join fellow technical and business leaders for an impactful day of learning and conversation at GitLab Commit 2022.  
    video:
        video_url: https://player.vimeo.com/video/739087277?h=b83676c803
        size: sm
  components:
    ## Add an 'anchor_id' value that matches corresponding navigation link above if section should be linkable
    - name: 'locations'
      data:
        anchor_id: 'locations'
        header: GitLab Commit 2022 Locations
        subtitle: Limited seats available. Reserve your spot today so you don’t miss out!
        locations:
        - title: New York City
          date: Thursday, September 22, 2022
          address: Spring Studios
          address_link: https://www.google.com/maps/place/Spring+Studios/@40.7208734,-74.0081928,17z/data=!3m1!4b1!4m5!3m4!1s0x89c2598ad4ed8177:0x7a3bfe10681cd1a5!8m2!3d40.7208734!4d-74.0060041
          img_url: /nuxt-images/events/nyc_mask.png
          img_alt: New York City skyline
          eventbriteId: 361094071087
          eventbriteLandingUrl: https://www.eventbrite.com/e/gitlab-commit-nyc-2022-tickets-361094071087
          register_button_text: Register for New York
          schedule_button_text: View New York Schedule
          schedule_button_url: http://commitnyc2022.sched.com/
        - title: London
          date: Tuesday, October 18, 2022
          address: National Centre for Circus Arts
          address_link: https://www.google.com/maps/place/National+Centre+for+Circus+Arts/@51.5275347,-0.0847467,17z/data=!3m1!4b1!4m5!3m4!1s0x48761ca52fd672ab:0x237057a9deca3b18!8m2!3d51.5275314!4d-0.082558
          img_url: /nuxt-images/events/london_mask.png
          img_alt: London skyline
          eventbriteId: 361108363837
          eventbriteLandingUrl: https://www.eventbrite.com/e/gitlab-commit-london-2022-tickets-361108363837"
          register_button_text: Register for London
          schedule_button_text: View London Schedule
          schedule_button_url: https://commitlondon2022.sched.com/


    - name: 'content-block'
      data:
        variant: imageVariant
        header: Get new ideas and best practices
        img_url: /nuxt-images/events/commit-new-ideas.png
        text: |
          Learn new ways to transform the way you deliver software with focused sessions in the Innovation Track and Technical Track, customer conversations, and keynote presentations — all from leaders at GitLab and prominent enterprises like Lockheed Martin and USAA, to name a few. Check out the schedule for [New York](https://commitnyc2022.sched.com/){data-ga-name="new york schedule" data-ga-location="body"} or [London](https://commitlondon2022.sched.com/){data-ga-name="london schedule" data-ga-location="body"} for more details.
        buttons:
          - variant: primary
            text: View New York Schedule
            link: https://commitnyc2022.sched.com/
            data_ga_name: new york schedule
            data_ga_location: body button

          - variant: primary
            text: View London Schedule
            link: https://commitlondon2022.sched.com/
            data_ga_name: london schedule
            data_ga_location: body button

    - name: 'content-block'
      data:
        variant: imageVariant
        header: Connect with your people + GitLab
        img_url: /nuxt-images/events/commit-connect.png
        text: |
          This is your opportunity to meet and connect with peers, industry leaders, and the people behind GitLab. Don’t miss the various networking opportunities built into the schedule — from breakfast and breaks to a rooftop networking mixer. Plus, there will be multiple ways for you to interact with GitLab and do everything from ask questions to participate in UX research.

    - name: 'speakers'
      data:
        anchor_id: 'speakers'
        background_color: '#5d5984'
        dropdown_label: Locations
        dropdown_options:
          - option_name: New York City
            value: NYC
          - option_name: London
            value: London

        speakers:
          - name: Sid Sijbrandij
            link: https://about.gitlab.com/handbook/ceo/
            tags: NYC, London
            title: Co-founder and CEO
            company: GitLab
            img_url: /nuxt-images/events/sid-sijbrandij-speaker-photo.png
          - name: Ashley Kramer
            link: https://about.gitlab.com/company/team/e-group/
            tags: NYC, London
            title: Chief Marketing and Strategy Officer
            company: GitLab
            img_url: /nuxt-images/events/ashley-kramer-speaker-photo.png
          - name: David DeSanto
            link: https://gitlab.com/david
            tags: NYC, London
            title: VP, Product
            company: GitLab
            img_url: /nuxt-images/events/david-desanto-speaker-photo.png
          - name: Aja Hammerly
            link: https://www.linkedin.com/in/ajahammerly/
            tags: NYC
            title: Developer Relations Manager
            company: Google Cloud
            img_url: /nuxt-images/events/aja-hammerly-speaker-photo.jpg
          - name: Alan Hohn
            link: https://www.linkedin.com/in/alan-hohn-5a3b3527/
            tags: NYC
            title: Director, Software Strategy
            company: Lockheed Martin
            img_url: /nuxt-images/events/alan-hohn-speaker-photo.png
          - name: Scott Crouch
            link: http://www.linkedin.com/in/crouchscott
            tags: NYC
            title: Principal Technical Architect
            company: USAA
            img_url: /nuxt-images/events/scott-crouch-speaker-photo.png
          - name: Lee Tickett
            link: https://www.linkedin.com/in/leetickett?originalSubdomain=uk
            tags: NYC
            title: Director
            company: Tickett Enterprises Limited
            img_url: /nuxt-images/events/lee-tickett-speaker-photo.jpg
          - name: Fernando Diaz
            link: https://gitlab.com/fjdiaz
            tags: NYC, London
            title: Senior Technical Marketing Manager
            company: GitLab
            img_url: /nuxt-images/events/fernando-diaz-speaker-photo.png
          - name: Francis Ofungwu
            link: https://gitlab.com/fofungwu
            tags: NYC, London
            title: Global Field CISO
            company: GitLab
            img_url: /nuxt-images/events/francis-ofungwu-speaker-photo.png
          - name: William Arias
            link: https://gitlab.com/warias
            tags: NYC, London
            title: Senior Technical Marketing Manager
            company: GitLab
            img_url: /nuxt-images/events/william-arias-speaker-photo.png
          - name: Laura Clymer
            link: https://www.linkedin.com/in/laura-clymer/
            tags: NYC, London
            title: Director of Marketing Research and Insights
            company: GitLab
            img_url: /nuxt-images/events/laura-clymer-speaker-photo.png
          - name: Stephen Walters
            link: https://www.linkedin.com/in/1stephenwalters/
            tags: NYC, London
            title: Field CTO (EMEA)
            company: GitLab
            img_url: /nuxt-images/events/stephen-walters-speaker-photo.png
          - name: Sam White
            link: https://www.linkedin.com/in/samuelowhite/
            tags: NYC
            title: Principal Product Manager
            company: GitLab
            img_url: /nuxt-images/events/sam-white-speaker-photo.png
          - name: Sherrod Patching
            link: https://www.linkedin.com/in/sherrodpatching/
            tags: NYC
            title: VP, Customer Success Management
            company: GitLab
            img_url: /nuxt-images/events/sherrod-patching-speaker-photo.jpg
          - name: Brian Wald
            link: https://www.linkedin.com/in/brianwald/
            tags: NYC
            title: Director, Solutions Architecture
            company: GitLab
            img_url: /nuxt-images/events/brian-wald-speaker-photo.jpg
          - name: Cesar Saavedra
            link: http://www.linkedin.com/in/saavedracesar
            tags: NYC, London
            title: Senior Technical Marketing Manager
            company: GitLab
            img_url: /nuxt-images/events/cesar-saavedra-speaker-photo.jpg
          - name: Kendra Marquart
            link: https://www.linkedin.com/in/kendra-marquart-5816b368/
            tags: NYC, London
            title: Sr. Manager of Global Curriculum Development
            company: GitLab
            img_url: /nuxt-images/events/kendra-marquart-speaker-photo.jpg
          - name: Brendan O'Leary
            link: https://www.linkedin.com/in/olearycrew
            tags: NYC,
            title: Staff Developer Evangelist
            company: GitLab
            img_url: /nuxt-images/events/brendan-oleary-speaker-photo.jpg

        callout:
          header: Interested in being a speaker?
          text: If you have an idea for a session related to DevOps, security, or application development, we'd love to hear it! We have a rolling call for proposals (CFP) for Commit conferences throughout the year.
          button_text: Submit your talk
          button_url: https://docs.google.com/forms/d/e/1FAIpQLSdFkBUNc7r47IEyuKcVbzYT26h8PDm7Av5yzqOVE_9OrZLiwA/viewform?usp=send_form
          data_ga_name: submit your talk
          data_ga_location: commit registration body

    - name: 'Carousel'
      data:
        header: Watch previous Commits
        videos:
          - title: "GitLab and UBS - Driving Innovation in the Financial Industry"
            photourl: /nuxt-images/events/gitlab-ubs-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/Tof-7fDultw
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "GitLab Secure & Protect: Bringing DevSecOps to Life"
            photourl: /nuxt-images/events/devsecops-to-life-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/InzXgpDuHJM
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "Innovation in Education"
            photourl: /nuxt-images/events/innovation-education-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/zRoQMrcxVB8
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "Scaling Collaboration for Remote Teams"
            photourl: /nuxt-images/events/remote-teams-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/8b9nWY45bEw
            carousel_identifier:
              - 'Commit 2021 Playlist'

          - title: "From Waterfall to Agile to DevOps with GitLab"
            photourl: /nuxt-images/events/waterfall-devops-thumbnail.jpeg
            video_link: https://www.youtube-nocookie.com/embed/5FwL9nZSFn0
            carousel_identifier:
              - 'Commit 2021 Playlist'


    - name: 'Faq'
      data:
          anchor_id: faq
          header: Frequently asked questions
          groups:
            - header: General
              questions:
                - question: Who should attend GitLab Commit?
                  answer: >-
                    GitLab Commit is for everyone interested or involved in DevOps – from the technology champions responsible for building, operating and securing applications to the technology executive driving transformation.

                - question: When can I register for GitLab Commit?
                  answer: >-
                    Registration will open on July 20, 2022

                - question: When and where will in-person GitLab Commit 2022 events be located?
                  answer: |
                    NYC: September 22, 2022 | [Spring Studios](https://www.springstudios.com/) 50 Varick St New York, NY 100013

                    London: October 18, 2022 | [National Centre for Circus Arts](https://www.nationalcircus.org.uk/) Coronet Street London N1 6HD United Kingdom

                - question: How much do in-person GitLab Commit passes cost?
                  answer: |
                    Commit NYC: $99 for early bird registration starting July 20-Aug 27 (11:59pm ET). Prices will go up on August 28th to $149. They will remain at this price until the event date or until we are sold out.

                    Commit London: £85 for early bird registration starting July 20-Sep 22 (11:59pm BT). Prices will go up on September 23rd to £125. They will remain at this price until the event date or until we are sold out.
                    
                - question: How can I attend Virtual Commit?
                  answer: >-
                    Virtual Commit event date, details, and registration will be coming soon!
                - question: What content tracks will you have?
                  answer: |
                      **Innovation track** - Hear from business leaders, industry experts, and GitLab customers on the present and future of DevOps

                      **Technical/Developer Labs track** - Break sessions that show how to use The One DevOps Platform for core use cases (e.g. Software Supply Chain Security, Point Solution Integration and Displacement)

                - question: Will all Commit attendees receive swag?
                  answer: >-
                    For NYC and London Commit events, we will have an on-site swag area where you can stop by to pick up your exclusive Commit swag during the event. For our Virtual Commit event, we will announce swag updates at a later time.

            - header: Travel
              questions:
                - question: Are there discounted hotel rooms for those traveling to in-person Commit events?
                  answer: >-
                    No, we are not anticipating hotel needs for attendees as most will be local.

            - header: Health and Safety
              questions:
                - question: What is GitLab doing for the health and safety of Commit participants?
                  answer: |
                    We prioritize health and safety and have planned smaller, limited-capacity (300-500 max depending on the location) in-person Commit events to ensure that everyone feels comfortable. Despite our best efforts, we understand that some may not feel comfortable joining us in a larger-scale event, so please remember that we will also have a Virtual Commit event likely in early 2023 so you may join us remotely from anywhere around the world. We want everyone to feel comfortable attending whether that’s from home or in-person.

                    To view the most updated United States travel information, please visit the [government travel website](https://travel.state.gov/content/travel/en/traveladvisories/covid-19-travel-information.html). For more information for UK travel information & policies please visit the [government travel website](https://www.gov.uk/coronavirus).

                - question: What about security?
                  answer: >-
                    We partner with a security team to work with all of our venues and consult on the program as a whole to ensure attendee safety. Please follow all directions provided by our specialized onsite security team. The security team will be available to attendees if anyone would like to directly raise any concerns onsite. More information about the onsite security team will be provided leading up to the event and while onsite.

                - question: I have dietary restrictions, will they be accommodated for meals on-site?
                  answer: >-
                    Our in-person event catering and menus will take into consideration all dietary restrictions indicated. Please ensure to indicate your dietary restrictions in the registration form.

                - question: What are the GitLab Commit Codes of Conduct?
                  answer: >-
                    We want all attendees to be able to participate fully in our Commit events, whether it's virtually or in-person. Every Commit attendee agrees to abide by the [Code of Business Conduct and Ethics, Events Code of Conduct, and Anti-Harassment Policy](https://about.gitlab.com/company/culture/ecoc/).

                - question: Do I need to be fully vaccinated for COVID to attend GitLab Commit?
                  answer: >-
                    Yes, full vaccination and a booster shot is required. See below for our COVID-19 policies.

            - header: COVID-19
              questions:
                - question: What precautions are we taking?
                  answer: >-
                    We’re adhering to the recommended guidelines from the Centers for Disease Control and Prevention (CDC), the World Health Organization (WHO), and local regulations to implement safety protocols accordingly. We strategically selected a venue that enables us to use a combination of outdoor spaces and multiple rooms to accommodate our large group. Furthermore, we are working on plans to ensure all attendees maintain appropriate social distancing as needed. Attendees will be required to wear masks, and we plan to offer hand washing and sanitizing stations throughout the event spaces. At this time, a Covid vaccination is mandatory to attend Commit onsite for speakers, attendees and GitLab team members. We’re monitoring this evolving situation closely, and we’ll release more specific guidelines and timelines from time to time.

                - question: Is a Covid-19 vaccination mandatory?
                  answer: >-
                    We are following local regulations closely to ensure safety. As of this time, a Covid vaccination is mandatory to attend Commit onsite for speakers,  attendees and GitLab team members. Please note as well that the US government requires non-U.S. citizen, nonimmigrant passengers arriving from a foreign country to the United States by air to be fully vaccinated. Please review [the CDC website](https://www.cdc.gov/coronavirus/2019-ncov/travelers/proof-of-vaccination.html) and the [gov.uk](https://www.gov.uk/coronavirus) for the most current information.

                - question: Is testing mandatory?
                  answer: >-
                    If testing is required at the time of Commit, GitLab will organize testing during the event for ticketed guests to adhere to local health monitoring procedures. This section will be updated as regulations and requirements evolve.

                - question: What if the newest COVID variant warrants another lockdown period, will there be a decision date on whether Commit will be exclusively virtual?
                  answer: >-
                    We will continue to monitor the Covid-19 situation closely, and we’ll make a decision closer to the event if travel restrictions and safety concerns indicate that an onsite event would not be in attendees’ best interest. We will continue to update everyone accordingly.


            - header: Sponsorships
              questions:
                - question: Are there sponsorship opportunities available?
                  answer: >-
                      We have a select group of sponsorships available, email us at [commit@gitlab.com](mailto:commit@gitlab.com) to inquire.

            - header: CFP/Speakers
              questions:
                - question: Where can I find the CFP to submit for a talk?
                  answer: >-
                    Please fill out the [GitLab Commit CFP](https://docs.google.com/forms/d/e/1FAIpQLSdFkBUNc7r47IEyuKcVbzYT26h8PDm7Av5yzqOVE_9OrZLiwA/viewform) form to submit an idea for a talk starting July 20th, 2022. Our CFP committee will be reviewing the submissions on a rolling basis throughout the year.

                - question: Which locations are talks being accepted?
                  answer: >-
                    We are currently accepting talks for Commit New York, London, and Virtual.

                - question: What session topics are you looking for in a submitted talk?
                  answer: >-
                    We are accepting talks that align with the following topics: Community of DevOps, DevOps platform, DevOps Thought leadership, GitOps, One DevOps Platform versus DIY Tool, Security & Compliance, Software Supply Chain Security, or Value Stream Management.

                - question: I’ve submitted a talk. When will I know if it’s been accepted?
                  answer: >-
                    As this is a rolling call-for-papers, response times may vary depending on when the CFP was submitted.

                - question: What if I have additional questions regarding the CFP?
                  answer: >-
                    Please reach out to commit@gitlab.com.

            - header: Accessibility and Inclusion
              questions:

                - question: What are my accessibility options?
                  answer: >-
                    To help attendees feel comfortable joining Commit, we will have the following options available:

                    - Accessible rooms
                    - Closed Captioning for Virtual Commit
                    - All-gender restrooms available on-site
                    - Personal pronoun indicator stickers/pins

                - question: How do I request accessibility services?
                  answer: >-
                    Please reach out to commit@gitlab.com

            - header: Sustainability and Corporate Social Responsibility
              questions:
                - question: What is GitLab doing in terms of sustainability and corporate social responsibility?
                  answer: >-
                    The planning team is committed to sustainability when possible. We are looking into how sustainability can play a role in all aspects when planning for Commit.

            - header: Contact Us
              questions:
                - question: How can I contact the GitLab Commit team if I have more questions?
                  answer: >-
                    Please reach out to commit@gitlab.com
    - name: 'sponsors'
      data:
        sponsors:
          - name: Google Cloud
            img: /nuxt-images/events/google-cloud.svg
            link: https://cloud.google.com/


    - name: 'cta-box'
      data:
        anchor_id: sponsors
        background_color: '#5D5984'
        header: Interested in sponsoring GitLab Commit?
        text: We have a select group of sponsorships available.
        button_text: Email us at commit@gitlab.com
        button_url: 'mailto:commit@gitlab.com'
        data_ga_name: email commit@gitlab.com
        data_ga_location: commit registration body

    - name: 'content-block'
      data:
        header: Code of Conduct
        text: |
          GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](https://about.gitlab.com/company/culture/ecoc/){data-ga-name="code of conduct" data-ga-location="body"} to ensure Commit is a friendly, inclusive, and comfortable environment for all participants.
