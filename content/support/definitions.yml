---
title: Support Definitions
description: Support Definitions
support-hero:
  data:
    title: Support Definitions
side_menu:
  anchors:
    text: 'ON THIS PAGE'
    data:
      - text: 'Definitions of Business Impact'
        href: '#definitions-of-business-impact'
        nodes:
          - text: 'Severity 4'
            href: '#severity-4'
          - text: 'Severity 3'
            href: '#severity-3'
          - text: 'Severity 2'
            href: '#severity-2'
          - text: 'Severity 1'
            href: '#severity-1'
          - text: 'Ticket severity and customer priority'
            href: '#ticket-severity-and-customer-priority'
      - text: Definition of Scaled Architecture
        href: "#definition-of-scaled-architecture"
  hyperlinks:
    text: ''
    data: []
components:
  - name: support-copy
    data:
      block:
        - header: Definitions of Support Impact
          id: definitions-of-support-impact
          text: |
            <p>The SLA times listed are the time frames in which you can expect the first response.</p>
        - subtitle:
            id: severity-4
            text: Severity 4
          text: |
            <p>
              Questions or Clarifications around features or documentation or deployments (24 hours) Minimal or no Business Impact.
              Information, an enhancement, or documentation clarification is requested, but there is no impact on the operation of GitLab.
              Implementation or production use of GitLab is continuing and work is not impeded. Example: A question about enabling ElasticSearch.
            </p>
        - subtitle:
            id: severity-3
            text: Severity 3
          text: |
            <p>
              Something is preventing normal GitLab operation (8 hours) Some Business Impact. Important GitLab features are unavailable or somewhat slowed, but a workaround is available.
              GitLab use has a minor loss of operational functionality, regardless of the environment or usage. Example: A known bug impacts the use of GitLab, but a workaround is successfully being used as a
              temporary solution.
            </p>
        - subtitle:
            id: severity-2
            text: Severity 2
          text: |
            <p>
              GitLab is Highly Degraded (4 hours) Significant Business Impact. Important GitLab features are unavailable or extremely slowed, with no acceptable workaround. Implementation or production use of GitLab is continuing;
              however, there is a serious impact on productivity. Example: CI Builds are erroring and not completing successfully, and the software release process is significantly affected.
            </p>
        - subtitle:
            id: severity-1
            text: Severity 1
          text: |
            <p>
              Your instance of GitLab is unavailable or completely unusable (30 Minutes) A GitLab server or cluster in production is not available, or is otherwise unusable. An emergency ticket can be filed and our On-Call Support
              Engineer will respond within 30 minutes. Example: GitLab showing 502 errors for all users.
            </p>
        - subtitle:
            id: ticket-severity-and-customer-priority
            text: Ticket severity and customer priority
          text: |
            <p>
              When submitting a ticket to support, you will be asked to select both a severity and a priority. The severity will display definitions aligning with <a href="support/definitions/#definitions-of-business-impact">the above section</a>. Priority is lacking any definition - this is because the priority
              is whatever you and your organization define it as. The customer priority (shorted on the forms as priority) is Support's way of asking the impact or importance the ticket has to your organization and its needs (business, technical, etc.).
            </p>
            <br />
            <p>
              <strong>Note:</strong> The <a href="https://federal-support.gitlab.com/">US Federal support portal</a> is often setup differently than the <a href="https://support.gitlab.com/">Global support portal</a> and might differ in the questions asked via the ticket submission forms.
            </p>

        - header: Definition of Scaled Architecture
          id: definition-of-scaled-architecture
          text: |
            <p>Scaled architecture is defined as any GitLab installation that separates services for the purposes of resilience, redundancy or scale. As a guide, our <a href="https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html">2,000 User Reference Architecture</a> (and higher) would fall under this category.</p>
            <br />
            <p><a href="/support/#priority-support">Priority Support</a> is required to receive assistance in troubleshooting a scaled implementation of GitLab.</p>
            <br />
            <p>In Omnibus and Source installations, Scaled Architecture is any deployment with multiple GitLab application nodes. This does not include external services such as Amazon RDS or ElastiCache.</p>
            <br />
            <table>
              <thead>
                <tr>
                  <th>Service</th>
                  <th>Scaled Architecture</th>
                  <th>Not Scaled Architecture</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Application (GitLab Rails)</td>
                  <td> Using multiple application nodes to provide resilience, scalability or availability</td>
                  <td>Using a single application node</td>
                </tr>
                <tr>
                  <td>Database</td>
                  <td>Using multiple database servers with Consul and PgBouncer</td>
                  <td>Using a single separate database or managed database service</td>
                </tr>
                <tr>
                  <td>Caching</td>
                  <td>Using Redis HA across multiple servers with Sentinel</td>
                  <td>Using a single separate server for Redis or a managed Redis service</td>
                </tr>
                <tr>
                  <td>Repository / Object Storage</td>
                  <td>Using one or more separate Gitaly nodes. Using NFS across multiple application servers.</td>
                  <td>Storing objects in S3</td>
                </tr>
                <tr>
                  <td>Job Processing</td>
                  <td>Using one or more separate Sidekiq nodes</td>
                  <td>Using Sidekiq on the same host as a single application node</td>
                </tr>
                <tr>
                  <td>Load Balancing</td>
                  <td>Using a Load Balancer to balance connections between multiple application nodes</td>
                  <td>Using a single application node</td>
                </tr>
                <tr>
                  <td>Monitoring</td>
                  <td>Not considered in the definition of Scaled Architecture</td>
                  <td>Not considered in the definition of Scaled Architecture</td>
                </tr>
              </tbody>
            </table>
